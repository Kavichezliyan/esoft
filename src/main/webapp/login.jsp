<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">

<head>
	<title>eSoft Login</title>
<!--	<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">-->
	
	<link href="https://fonts.googleapis.com/css?family=Lato:400,700,900|Londrina+Shadow|Montserrat|Nunito:400,600,700,900|Slabo+27px" rel="stylesheet">
	
	<link href="${contextPath}/resources/css/login.css" type="text/css" rel="stylesheet">


<!--
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
-->
</head>
	
<body class="container">
<header class="logo" role="banner">
	<a class="title-header" href="${contextPath}/index.jsp">eSoft</a>
</header>	
	
<div class="form-container">
	<form method="POST" action="${contextPath}/login" class="form-signin">
		
<!--
    		<div class="user">
    			<img src="M:\Websites\MadraS-Restaurant\Photos\profile%20avatar.png" class="avatar">
			</div>
-->	
<!--
	<div class="main">											<div id="id01" class="modal">
  			<span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>							
-->
		<div class="text-center">
		
			<h2>Sign In</h2>
			
		</div>
		<div class="form-group ${error != null ? 'has-error' : ''}">						
		<div class="field-set">
			<div class="field-message">${message}</div>
			<label for="login_field">Email:</label>
			<input class="form-control" type="text" name="username" placeholder="Your Email" autocomplete="off" autofocus="true" required>
 		
			<label for="Password ">Password:</label>
			<input class="form-control" type="Password" name="password" placeholder="Your Password" pattern=".{5,10}" 	title="5 to 10 characters" autocomplete="off" required>
			<div class="field-error">${error}</div>
			
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			
		</div>
		
			<button class="loginbtn" type="submit">Login</button>
<!--		<button type="button" class="btn-login-cancel">Cancel</button>-->
			
		<div>
		<label>
			<input type="checkbox" id="checkbox" name="remember">Remember Me</label>
			<span class="fgtpsw"><a href="#">Forgot Password?</a></span>
		</div>
		
		<div class="footer">	
			<p>Don't have an account?
			<a href="${contextPath}/registration">Sign Up</a></p>
		</div>
		</div>										
</form>
</div>
</body>
</html>






<!--
	<style type="text/css">
		body{
			background-image: image("Personal Website\debby-hudson-544846-unsplash.jpg");
			background-size: cover;
			background-position: center;
			background-attachment: fixed;
			font-family: 'Lato', sans-serif;
			color: white;
		}
	</style>