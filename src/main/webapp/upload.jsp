<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Upload</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link href="https://fonts.googleapis.com/css?family=Lato:400,700,900|Londrina+Shadow|Montserrat|Nunito:400,600,700,900|Slabo+27px" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/upload.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css">
</head>
<body>
	<nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">eSoft</a>
    </div>
 <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
       	<li><a href="${contextPath}/index">Home</a></li>
		<li><a href="#">About</a></li>
		<li><a href="#">Services</a></li>
		<li class="active"><a href="#">Upload</a></li>
      </ul>
<ul class="nav navbar-nav navbar-right">
       <li><a href="#">Contact</a></li>
       <li>
       	<c:if test="${pageContext.request.userPrincipal.name != null}">
       	<a href="#"> Welcome ${pageContext.request.userPrincipal.name} </a>
       	</c:if>
       	</li>
       <li>
        <a onclick="document.forms['logoutForm'].submit()">Logout
        <i class="fas fa-user"></i> </a>
    	</li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
		<div class="container">
                <!-- The fileinput-button span is used to style the file input field as button -->
                <span class="btn btn-success fileinput-button" id="custom-button" >
                    <i class="glyphicon glyphicon-plus"></i>
                    <span >Add files...</span>
                    <input type="file" id="real-file" name="files[]" multiple="" style="display:none">
                </span>
                <!--button type="button" class="btn btn-primary start"  id="start-button">
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start upload</span>
                </button>
                <button type="button" class="btn btn-warning cancel" id="cancel-button">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel upload</span>
                </button-->
				
                <!-- The global file processing state -->
                <span class="fileupload-process"></span>
        </div>
		
		<div class="container">
		</br>
		<span id="custom-text">No file chosen</span>
		</br></br>
		<p id="content-target"></p>
		</div>
		
	<!--div class="container">
		<input type="file" id="real-file" hidden="hidden"/>
		<button type="button" id="custom-button">CHOOSE FILE</button>
		<span id="custom-text">No file chosen</span>
		<button type="submit" id="submit-button">SUBMIT</button>
	</div-->
	 <form id="logoutForm" method="POST" action="${contextPath}/logout">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
     </form>
</body>

<script type="text/javascript" src="${contextPath}/resources/js/upload.min.js"></script>
<!--script type="text/javascript">
const realFileBtn = document.getElementById("real-file");
const customBtn = document.getElementById("custom-button");
const customTxt = document.getElementById("custom-text");
const startBtn = document.getElementById("start-button");

customBtn.addEventListener("click", function() {
  realFileBtn.click();
});

realFileBtn.addEventListener("change", function() {
  if (realFileBtn.value) {
    customTxt.innerHTML = realFileBtn.value.match(
      /[\/\\]([\w\d\s\.\-\(\)]+)$/
    )[1];
  } else {
    customTxt.innerHTML = "No file chosen";
  }
}); 
</script-->
</html>