package com.esoft.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.esoft.app.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long>{
}
