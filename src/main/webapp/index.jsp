<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">

<head>
	<title>eSoft</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

	<link href="https://fonts.googleapis.com/css?family=Lato:400,700,900|Londrina+Shadow|Montserrat|Nunito:400,600,700,900|Slabo+27px" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/index.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css">
</head>
<body>
	<nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">eSoft</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
       	<li class="active"><a href="${contextPath}/index">Home</a></li>
		<li><a href="#">About</a></li>
		<li><a href="#">Services</a></li>
		<li><a href="#">Upload</a></li>
      </ul>
      
      <ul class="nav navbar-nav navbar-right">
       <li><a href="#">Contact</a></li> 
       <li>
       <a href="/registration">Sign Up <i class="fas fa-user-plus"></i></a></li>
       <li><a href="/login">Sign In <i class="fas fa-user"></i></a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<div class="container">
	<div class="row"> 
		<div class="col-lg-12">
			<div class="content">
				<p>This is the sample content about the Attendance Registration using finger print. It records the attendance of the employees with time and date. We can record, log and upload the data anytime we want. We can fetch the selected data log.</p>
				<hr>
			</div>
		</div>
	</div>
</div>




<script src="http://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>

</body>
</html>